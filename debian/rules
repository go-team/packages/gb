#!/usr/bin/make -f

# this should be updated to match the golang-1.X-go package used in d/control
export PATH := /usr/lib/go-1.10/bin:$(PATH)

# dh-golang doesn't copy everything we might need (and some tests create garbage in test directories)
pristine-testdata:
	set -ex; for path in \
		internal/fileutils/_testdata \
		internal/untar/_testdata \
		internal/vendor/_testdata \
		testdata \
	; do \
		rsync -a --delete "$$path/" "_build/src/github.com/constabulary/gb/$$path/"; \
	done

override_dh_auto_test:
	./debian/rules pristine-testdata

	# some tests need network access (gated by -short), so we run those only via DEP8
	dh_auto_test -- -short

# this is used by "d/tests/go-test" (DEP8)
# it lives here to inherit the correct $PATH
run-network-tests:
	mkdir -p _build/src/github.com/constabulary/gb
	rsync -a --delete /usr/share/gocode/src/github.com/constabulary/gb/ _build/src/github.com/constabulary/gb/
	# the tests try to modify files, hence the copying
	GOPATH="$$PWD/_build:/usr/share/gocode" \
		go test -v github.com/constabulary/gb/...

override_dh_auto_install:
	# re-copy pristine testdata so we delete garbage created by build/test
	./debian/rules pristine-testdata

	dh_auto_install

%:
	dh $@ --buildsystem=golang --with=golang --builddirectory=_build
